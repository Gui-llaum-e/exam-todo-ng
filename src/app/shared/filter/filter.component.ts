import { Component } from '@angular/core';

export interface Todo {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent {
  todos: Todo[] = [
    {value: 'etiquette-0', viewValue: 'None'},
    {value: 'etiquette-1', viewValue: 'Etiquette 1'},
    {value: 'etiquette-2', viewValue: 'Etiquette 2'}
  ];
}
